#!/bin/bash

python3 -m pytest --disable-warnings --cov-report term-missing --cov=/app/src/numerous | tee results.out
if [[ "$PIPESTATUS" -eq "1" ]]; then EXIT_CODE=1; else EXIT_CODE=0; fi

exit $EXIT_CODE

