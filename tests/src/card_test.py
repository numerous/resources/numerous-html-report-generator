from numerous.html_report_generator import Card, Div, Report
import os.path
from pathlib import Path


def test_cards_in_report():
    folder = Path("./output")
    filename = 'test_cards_in_report'
    file = folder.joinpath(filename+'.html')
    if os.path.exists(file):
        os.remove(file)

    report = Report(target_folder=folder, filename=filename)
    report.add_header_info(header='Test report header - xyz',
                           title='Test report title',
                           sub_title='Test report sub title',
                           sub_sub_title='Test report sub sub title',
                           )

    card1 = Card("hello world!", classes='frame')


    report.add_blocks({'card1': card1})

    report.save()
    assert os.path.isfile(file)


def test_cards_add_content_in_report():
    folder = Path("./output")
    filename = 'test_cards_add_content_in_report'
    file = folder.joinpath(filename+'.html')
    if os.path.exists(file):
        os.remove(file)

    report = Report(target_folder=folder, filename=filename)
    report.add_header_info(header='Test report header - xyz',
                           title='Test report title',
                           sub_title='Test report sub title',
                           sub_sub_title='Test report sub sub title',
                           )

    card1 = Card("hello world!", classes="frame")
    card2 = Card("hello world again!", classes='subframe')
    div1 = Div('<h1>hello world</h1>')
    card2.add_content({'div1': div1})
    card1.add_content({'card2': card2})

    report.add_blocks({'card1': card1})

    report.save()
    assert os.path.isfile(file)


def test_cards_in_report_with_table(new_table):
    folder = Path("./output")
    filename = 'test_cards_in_report_with_table'
    file = folder.joinpath(filename+'.html')
    if os.path.exists(file):
        os.remove(file)

    report = Report(target_folder=folder, filename=filename)
    report.add_header_info(header='Test report header - xyz',
                           title='Test report title',
                           sub_title='Test report sub title',
                           sub_sub_title='Test report sub sub title',
                           )

    card1 = Card("hello world!", classes="frame")
    card2 = Card("hello world again!", classes='subframe')
    div1 = Div('<h1>hello world</h1>')
    card2.add_content({'div1': div1})
    card1.add_content({'card2': card2})


    report.add_blocks({'card1': card1})
    card2.add_content({"table": new_table("test1")})

    report.save()
    assert os.path.isfile(file)