from html.parser import HTMLParser
from numerous.html_report_generator import Report, Div, Section, Subsection

import pytest
import os.path
from pathlib import Path

@pytest.mark.parametrize('sectionClass', [Section, Subsection])
def test_section_init(sectionClass: type(Section)):

    sec = sectionClass("This is the section title")
    assert sec.section_title == "This is the section title"

    with pytest.raises(Exception) as e_info:
        sec = sectionClass()
    assert "__init__() missing 1 required positional argument: 'section_title'" in str(e_info.value)


@pytest.mark.parametrize('sectionClass', [Section, Subsection])
def test_section_in_report(sectionClass: type(Section)):
    folder = Path("./output")
    filename = 'test_section_in_report'
    file = folder.joinpath(filename+'.html')
    if os.path.exists(file):
        os.remove(file)

    report = Report(target_folder=folder, filename=filename)
    report.add_header_info(header = 'Test report header',
                           title = 'Test report title',
                           sub_title = 'Test report sub title',
                           sub_sub_title = 'Test report sub sub title',
                           )
    sec = sectionClass("This is the section title")
    sec.add_content({'line1': Div('<b>Bla bla bla bla</b>')})
    report.add_blocks({'sec1': sec})
    report.save()

    assert os.path.isfile(file)


@pytest.mark.parametrize('sectionClass', [Section, Subsection])
def test_section_in_report_with_table(sectionClass: type(Section), new_table):
    folder = Path("./output")
    filename = 'test_section_in_report_with_table'
    file = folder.joinpath(filename+'.html')
    if os.path.exists(file):
        os.remove(file)

    report = Report(target_folder=folder, filename=filename)
    report.add_header_info(header = 'Test report header',
                           title = 'Test report title',
                           sub_title = 'Test report sub title',
                           sub_sub_title = 'Test report sub sub title',
                           )
    sec = sectionClass("This is the section title")
    sec.add_content({'line1': Div('<b>Bla bla bla bla</b>')})

    sec.add_content({"table": new_table("test1")})
    report.add_blocks({'sec1': sec})
    report.save()

    assert os.path.isfile(file)


@pytest.mark.parametrize('sectionClass', [Section, Subsection])
def test_html_check(sectionClass: type(Section)):
    with pytest.raises(Exception) as e_info:
        sec = sectionClass("This is the section title")
        sec.add_content({'line1': 'Bla bla bla bla'})
        assert "Current string in content is not an html string:" in str(e_info.value)


@pytest.mark.parametrize('sectionClass', [Section, Subsection])
def test_set_functions(sectionClass: type(Section)):
    report = Report(target_folder=Path("output"))
    sec = Section("This is the section title")
    div_ = Div('<b>Bla bla bla bla</b>')
    sec.add_content({'line1': div_})
    report.add_blocks({'sec1': sec})

    assert sec.content['line1'] == div_
    assert report.blocks == {'sec1': sec}

    #Setting content of section only
    div2_ = Div('<b>La la la la</b>')
    sec.set_content({'line2': div2_})
    assert sec.content['line2'] == div2_
    assert report.blocks['sec1'].content['line2'] == div2_

    # Setting content of section and report
    div3_ = Div('<b>A a a a</b>')
    sec.set_content({'line3': div3_})
    report.set_blocks({'sec3': sec})
    assert sec.content['line3'] == div3_
    assert report.blocks['sec3'].content['line3'] == div3_


@pytest.mark.parametrize('sectionClass', [Section, Subsection])
def test_section_formatting(sectionClass: type(Section)):
    folder = Path("./output")
    filename = 'test_section_in_report'
    file = folder.joinpath(filename + '.html')
    report = Report(target_folder=folder, filename=filename)
    report.add_header_info(header='Test report header',
                           title='Test report title',
                           sub_title='Test report sub title',
                           sub_sub_title='Test report sub sub title',
                           )
    section_text = "This the section text - it must be unique here in this test"
    sec = sectionClass(section_text)
    report.add_blocks({'sec1': sec})
    report.save()

    if sectionClass == Section:
        headertag = 'h1'
    elif sectionClass == Subsection:
        headertag = 'h2'
    else:
        raise NotImplementedError


    with open(file, 'r') as f:
        html_report = f.read()

    class MyHTMLParser(HTMLParser):
        def __init__(self):
            super().__init__()
            self.header_tag_start = False
            self.found = False
        def handle_starttag(self, tag, attrs):
            if tag == headertag:
                self.header_tag_start = True

        def handle_endtag(self, tag: str):
            if tag == headertag:
                self.header_tag_start = False

        def handle_data(self, data: str) -> None:
            if self.header_tag_start:
                if data == section_text:
                    self.found = True

    parser = MyHTMLParser()
    parser.feed(html_report)

    assert parser.found, f"tag {headertag} not found"

