import pytest
from typing import Union

from numerous.html_report_generator import GoFigure
from numerous.html_report_generator import Report
import os.path
from pathlib import Path


@pytest.mark.parametrize('fig_data_', ["new_figure_definition", "new_figure_traces"])
def test_fig_in_report(fig_data_: Union[dict, list[dict]], request):
    fig_data = request.getfixturevalue(fig_data_)
    folder = Path("./output")
    filename = 'test_fig_in_report'
    file = folder.joinpath(filename+'.html')
    if os.path.exists(file):
        os.remove(file)

    report = Report(target_folder=folder, filename=filename)
    report.add_header_info(header='Test report header - xyz',
                           title='Test report title',
                           sub_title='Test report sub title',
                           sub_sub_title='Test report sub sub title',
                           )

    fig = GoFigure(fig_data, caption="test")

    report.add_blocks({'fig': fig})

    report.save()
    assert os.path.isfile(file)

def test_update_figure_layout(new_figure_traces):
    folder = Path("./output")
    filename = 'test_fig_in_report'
    file = folder.joinpath(filename + '.html')
    if os.path.exists(file):
        os.remove(file)

    report = Report(target_folder=folder, filename=filename)
    report.add_header_info(header='Test report header - xyz',
                           title='Test report title',
                           sub_title='Test report sub title',
                           sub_sub_title='Test report sub sub title',
                           )

    fig = GoFigure(figure_data=new_figure_traces, caption="test")

    report.add_blocks({'fig': fig})
    fig.figure_obj.update_layout({"title": {"text": "A Figure Specified By Python Dictionary"}})

    report.save()
    assert os.path.isfile(file)


@pytest.mark.parametrize('fig_data_', ["new_figure_definition", "new_figure_traces"])
def test_fig_numbering(fig_data_: Union[dict, list[dict]], request):
    fig_data = request.getfixturevalue(fig_data_)
    folder = Path("./output")
    filename = 'test_fig_numbering'
    file = folder.joinpath(filename+'.html')
    if os.path.exists(file):
        os.remove(file)

    report = Report(target_folder=folder, filename=filename)
    report.add_header_info(header='Test report header - xyz',
                           title='Test report title',
                           sub_title='Test report sub title',
                           sub_sub_title='Test report sub sub title',
                           )

    fig_a = GoFigure(fig_data, caption="test")

    report.add_blocks({'fig_a': fig_a})

    fig_b = GoFigure(fig_data, caption="test")

    report.add_blocks({'fig_b': fig_b})

    report.save()

    with open(file, "r") as f:
        html_content = f.read()

    assert "Figure 1" in html_content
    assert "Figure 2" in html_content


@pytest.mark.parametrize('fig_data_', ["new_figure_definition", "new_figure_traces"])
def test_fig_no_caption(fig_data_: Union[dict, list[dict]], request):
    fig_data = request.getfixturevalue(fig_data_)
    folder = Path("./output")
    filename = 'test_fig_no_caption'
    file = folder.joinpath(filename+'.html')
    if os.path.exists(file):
        os.remove(file)

    report = Report(target_folder=folder, filename=filename)
    report.add_header_info(header='Test report header - xyz',
                           title='Test report title',
                           sub_title='Test report sub title',
                           sub_sub_title='Test report sub sub title',
                           )

    fig_a = GoFigure(fig_data)

    report.add_blocks({'fig_a': fig_a})

    fig_b = GoFigure(fig_data)

    report.add_blocks({'fig_b': fig_b})

    report.save()

    with open(file, "r") as f:
        html_content = f.read()

    assert "Figure 1" not in html_content
    assert "Figure 2" not in html_content


def test_fig_as_bitmap(new_figure_traces):
    folder = Path("./output")
    filename = 'test_fig_as_bitmap'
    file = folder.joinpath(filename+'.html')
    if os.path.exists(file):
        os.remove(file)

    report = Report(target_folder=folder, filename=filename)
    report.add_header_info(header='Test report header - xyz',
                           title='Test report title',
                           sub_title='Test report sub title',
                           sub_sub_title='Test report sub sub title',
                           )

    fig_a = GoFigure(new_figure_traces, caption="Test figure1")

    report.add_blocks({'fig_a': fig_a})

    fig_b = GoFigure(new_figure_traces, caption="Test figure2")

    report.add_blocks({'fig_b': fig_b})

    report.save(images_as_bitmaps=True)

    with open(file, "r") as f:
        html_content = f.read()

    assert fig_a.id in html_content
    assert fig_b.id in html_content
    assert fig_a.image_name.exists()
    assert fig_b.image_name.exists()


def test_fig_width_and_height_bitmaps(new_figure_traces):
    def get_png_dimensions(file_path):
        with open(file_path, 'rb') as f:
            # Read the PNG header
            f.seek(16)  # Width and height are located at bytes 16-24
            width = int.from_bytes(f.read(4), 'big')
            height = int.from_bytes(f.read(4), 'big')
        return width, height

    folder = Path("./output")
    filename = 'test_fig_width_and_height_bitmaps'

    file = folder.joinpath(filename+'.html')

    if os.path.exists(file):
        os.remove(file)

    report = Report(target_folder=folder, filename=filename)
    report.add_header_info(
        header='Test report header - xyz',
        title='Test report title',
        sub_title='Test report sub title',
        sub_sub_title='Test report sub sub title',
    )

    fig_a = GoFigure(new_figure_traces, caption="Test figure1")

    report.add_blocks({'fig_a': fig_a})

    height = 2048
    width = 3508

    report.save(images_as_bitmaps=True, image_width=2048, image_height=3508)

    with open(file, "r") as f:
        html_content = f.read()

    assert fig_a.id in html_content
    assert fig_a.image_name.exists()

    height_, width_ = get_png_dimensions(fig_a.image_name)

    assert height_ == height
    assert width_ == width

