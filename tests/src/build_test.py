from .conftest import run_command
import sys
import os


def test_build_install_and_run_example(venv_dir):
    # Step 1: Create a virtual environment
    run_command(f"{sys.executable} -m venv {venv_dir}")

    # Define the paths for Python executable and pip inside the new venv
    if os.name == "nt":
        python_exec = os.path.join(venv_dir, "Scripts", "python")
        pip_exec = os.path.join(venv_dir, "Scripts", "pip")
    else:
        python_exec = os.path.join(venv_dir, "bin", "python")
        pip_exec = os.path.join(venv_dir, "bin", "pip")

    # Step 2: Install the "build" package in the virtual environment
    run_command(f"{pip_exec} install build", check=True)

    # Step 3: Run "build" to create the wheel file
    # Set working directory to the project root where pyproject.toml is located
    project_root = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../"))
    run_command(f"{python_exec} -m build", cwd=project_root, check=True)

    # Find the built wheel file (assumes output is in "dist" directory at root)
    dist_dir = os.path.join(project_root, "dist")
    wheel_files = [f for f in os.listdir(dist_dir) if f.endswith(".whl")]
    assert wheel_files, "No wheel file found in 'dist' directory"
    wheel_path = os.path.join(dist_dir, wheel_files[0])

    # Step 4: Install the built wheel in the virtual environment
    run_command(f"{pip_exec} install {wheel_path}", check=True)

    # Step 5: Run the "example.py" script inside the virtual environment
    # example.py is located in /tests/src, so we specify its path accordingly
    example_script_path = os.path.join(project_root, "tests", "src", "example.py")
    assert os.path.exists(example_script_path), f"Script not found: {example_script_path}"

    # Run the example.py script and capture the exit code
    exit_code = run_command(f"{python_exec} {example_script_path}")

    # Assert that the script exited with a non-zero code
    assert exit_code == 0, f"Expected zero exit code, got {exit_code}."
