from numerous.html_report_generator import Report
import os.path
from pathlib import Path


def test_table_in_report(new_table):
    folder = Path("./output")
    filename = 'test_table_in_report'
    file = folder.joinpath(filename+'.html')
    if os.path.exists(file):
        os.remove(file)

    report = Report(target_folder=folder, filename=filename)
    report.add_header_info(header='Test report header - xyz',
                           title='Test report title',
                           sub_title='Test report sub title',
                           sub_sub_title='Test report sub sub title',
                           )

    report.add_blocks({'table': new_table("test")})

    report.save()
    assert os.path.isfile(file)


def test_table_numbering(new_table):
    folder = Path("./output")
    filename = 'test_table_numbering'
    file = folder.joinpath(filename+'.html')
    if os.path.exists(file):
        os.remove(file)

    report = Report(target_folder=folder, filename=filename)
    report.add_header_info(header='Test report header - xyz',
                           title='Test report title',
                           sub_title='Test report sub title',
                           sub_sub_title='Test report sub sub title',
                           )

    report.add_blocks({'table_a': new_table("test1")})

    report.add_blocks({'table_b': new_table("test2")})

    report.save()

    with open(file, "r") as f:
        html_content = f.read()

    assert "Table 1" in html_content
    assert "Table 2" in html_content


def test_table_no_caption(new_table):
    folder = Path("./output")
    filename = 'test_table_no_caption'
    file = folder.joinpath(filename+'.html')
    if os.path.exists(file):
        os.remove(file)

    report = Report(target_folder=folder, filename=filename)
    report.add_header_info(header='Test report header - xyz',
                           title='Test report title',
                           sub_title='Test report sub title',
                           sub_sub_title='Test report sub sub title',
                           )

    report.add_blocks({'table_a': new_table()})

    report.add_blocks({'table_b': new_table()})

    report.save()

    with open(file, "r") as f:
        html_content = f.read()

    assert "Table 1" not in html_content
    assert "Table 2" not in html_content
