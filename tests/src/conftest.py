import pytest
import pandas as pd
from typing import Optional
import tempfile
import os
import subprocess
from numerous.html_report_generator import DataFrameTable
import plotly.graph_objects as go


def run_command(command, env=None, cwd=None, check=False):
    """Helper function to run a shell command and return exit code and output."""
    result = subprocess.run(
        command,
        shell=True,
        text=True,
        env=env,
        cwd=cwd  # Set the working directory
    )
    if check and result.returncode != 0:
        raise subprocess.CalledProcessError(
            result.returncode, command, output=result.stdout, stderr=result.stderr
        )
    return result.returncode


@pytest.fixture(scope="function")
def venv_dir():
    # Create a temporary directory for the virtual environment
    with tempfile.TemporaryDirectory() as tmpdirname:
        venv_path = os.path.join(tmpdirname, "venv")
        yield venv_path  # Provide the virtual environment path to the test


@pytest.fixture
def new_table():
    table_data = pd.DataFrame({
        'a': [1, 2, 3],
        'b': [5, 6, 7]
    })

    def fn(caption: Optional[str] = None):

        return DataFrameTable(table_data, caption=caption)

    yield fn


@pytest.fixture
def new_figure_definition():
    fig_data1 = {
        "data": [{"type": "bar",
                  "x": [1, 2, 3],
                  "y": [1, 3, 2]}],
        "layout": {"title": {"text": "A Figure Specified By Python Dictionary"}}
    }
    yield fig_data1


@pytest.fixture
def new_figure_traces():

    fig_data2 = [go.Scatter({'x': [1, 2, 3, 4], 'y': [2, -1, 10, 1]})]
    yield fig_data2

