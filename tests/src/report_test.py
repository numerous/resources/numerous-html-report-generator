import os.path
import pytest
from numerous.html_report_generator.report import Report
from pathlib import Path
from datetime import datetime

def test_report_init():

    #Test default values
    with pytest.raises(AttributeError, match=r"Target folder is required"):
        Report()

    # Test custom values
    report = Report(
        target_folder=Path('test/test/test'),
        template=Path('test/test/test/templates.html'),
        filename='test_report'
    )

    assert report.target_folder == Path('test/test/test')
    assert report.template == Path('test/test/test/templates.html')
    assert report.filename == 'test_report.html'


def test_report_header_info():

    report = Report(target_folder=Path("output"))
    report.add_header_info(
        header='Test report header',
        title='Test report title',
        sub_title='Test report sub title',
        sub_sub_title='Test report sub sub title',
        footer_title='Test footer title',
        footer_content='Test footer content',
    )

    assert report.report_header_info['report_header'] == 'Test report header'
    assert report.report_header_info['report_title'] == 'Test report title'
    assert report.report_header_info['report_type_title'] == 'Test report header'
    assert report.report_header_info['report_sub_title'] == 'Test report sub title'
    assert report.report_header_info['report_sub_sub_title'] == 'Test report sub sub title'
    assert report.report_header_info['report_date'] == "{: %d-%m-%Y}".format(datetime.now())


def test_report_to_html():

    folder = Path("./output")
    filename = 'test_report_to_html'
    file = folder.joinpath(filename+'.html')
    if os.path.exists(file):
        os.remove(file)

    report = Report(target_folder=folder, filename=filename)
    report.add_header_info(
        header='Test report header',
        title='Test report title',
        sub_title='Test report sub title',
        sub_sub_title='Test report sub sub title',
        footer_title="Test footer title",
        footer_content="Test footer content"
    )
    report.save()

    assert os.path.isfile(file)
