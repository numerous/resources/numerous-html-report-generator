from numerous.html_report_generator import Div
from numerous.html_report_generator import Report
import os.path
from pathlib import Path


def test_divs_in_report():
    folder = Path("./output")
    filename = 'test_divs_in_report'
    file = folder.joinpath(filename+'.html')
    if os.path.exists(file):
        os.remove(file)

    report = Report(target_folder=folder, filename=filename)
    report.add_header_info(header='Test report header - xyz',
                           title='Test report title',
                           sub_title='Test report sub title',
                           sub_sub_title='Test report sub sub title',
                           )

    divs1 = Div("hello world!")

    report.add_blocks({'div1': divs1})

    report.save()
    assert os.path.isfile(file)