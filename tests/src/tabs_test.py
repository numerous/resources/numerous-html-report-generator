from numerous.html_report_generator import DataFrameTable, Report, Tabs


import os.path
import pandas as pd
from pathlib import Path

def test_tabs_counter():
    tabs1 = Tabs()
    tabs2 = Tabs()
    tabs3 = Tabs()
    tabs4 = Tabs()

    assert tabs1.id == 0
    assert tabs2.id == 1
    assert tabs3.id == 2
    assert tabs4.id == 3


def test_tabs_in_report():
    folder = Path("./output")
    filename = 'test_tabs_in_report'
    file = folder.joinpath(filename+'.html')
    if os.path.exists(file):
        os.remove(file)

    report = Report(target_folder=folder, filename=filename)
    report.add_header_info(header='Test report header - xyz',
                           title='Test report title',
                           sub_title='Test report sub title',
                           sub_sub_title='Test report sub sub title',
                           )

    tabs1 = Tabs()
    tabs1.add_tabs({'Tab1 button': "<b>Tab1 content xyz</b>",
                   'Tab2 button': "<b>Tab2 content</b>",
                   'Tab3 button': "<b>Tab3 content</b>",
                   })

    report.add_blocks({'tab1': tabs1})


    tabs2 = Tabs()
    tabs2.add_tabs({'Tab4 button': "<b>Tab4 content xyz</b>",
                   'Tab5 button': "<b>Tab5 content</b>",
                   'Tab6 button': "<b>Tab6 content</b>",
                   })

    report.add_blocks({'tab2': tabs2})

    report.save()
    assert os.path.isfile(file)


def test_tabs_numbering(new_table):
    folder = Path("./output")
    filename = 'test_tabs_numbering'
    file = folder.joinpath(filename+'.html')
    if os.path.exists(file):
        os.remove(file)

    report = Report(target_folder=folder, filename=filename)
    report.add_header_info(header='Test report header - xyz',
                           title='Test report title',
                           sub_title='Test report sub title',
                           sub_sub_title='Test report sub sub title',
                           )

    tabs1 = Tabs()
    tabs1.add_tabs({'Tab1 button': "<b>Tab1 content xyz</b>",
                   'Tab2 button': "<b>Tab2 content</b>",
                   'Tab3 button': "<b>Tab3 content</b>",
                   })

    report.add_blocks({'tab1': tabs1})

    tabs1.add_tabs({"table_a": new_table("test")})

    tabs2 = Tabs()
    tabs2.add_tabs({"table_b": new_table("test")})
    tabs2.add_tabs({'Tab4 button': "<b>Tab4 content xyz</b>",
                   'Tab5 button': "<b>Tab5 content</b>",
                   'Tab6 button': "<b>Tab6 content</b>",
                   })

    report.add_blocks({'tab2': tabs2})

    report.save()
    with open(file, "r") as f:
        html_content = f.read()

    assert "Table 1" in html_content
    assert "Table 2" in html_content


def test_tabs_no_caption(new_table):
    folder = Path("./output")
    filename = 'test_tabs_no_caption'
    file = folder.joinpath(filename+'.html')
    if os.path.exists(file):
        os.remove(file)

    report = Report(target_folder=folder, filename=filename)
    report.add_header_info(header='Test report header - xyz',
                           title='Test report title',
                           sub_title='Test report sub title',
                           sub_sub_title='Test report sub sub title',
                           )

    tabs1 = Tabs()
    tabs1.add_tabs({'Tab1 button': "<b>Tab1 content xyz</b>",
                   'Tab2 button': "<b>Tab2 content</b>",
                   'Tab3 button': "<b>Tab3 content</b>",
                   })

    report.add_blocks({'tab1': tabs1})

    tabs1.add_tabs({"table_a": new_table()})

    tabs2 = Tabs()
    tabs2.add_tabs({"table_b": new_table()})
    tabs2.add_tabs({'Tab4 button': "<b>Tab4 content xyz</b>",
                   'Tab5 button': "<b>Tab5 content</b>",
                   'Tab6 button': "<b>Tab6 content</b>",
                   })

    report.add_blocks({'tab2': tabs2})

    report.save()
    with open(file, "r") as f:
        html_content = f.read()

    assert "Table 1" not in html_content
    assert "Table 2" not in html_content


def test_set_tabs():
    report = Report(target_folder=Path("output"))
    tabs = Tabs()
    tabs.add_tabs({'Tab1 button': "<b>Tab1 content</b>"})
    report.add_blocks({'tab1': tabs})

    assert tabs.tabs == {'Tab1 button': "<b>Tab1 content</b>"}
    assert report.blocks['tab1'].tabs == {'Tab1 button': "<b>Tab1 content</b>"}

    tabs.set_tabs({'Tab2 button': "<b>Tab2 content</b>"})

    assert tabs.tabs == {'Tab2 button': "<b>Tab2 content</b>"}
    assert report.blocks['tab1'].tabs == {'Tab2 button': "<b>Tab2 content</b>"}