to build the docs manually use:

```
sphinx-apidoc -o ./source ../src/numerous
sphinx-build -b html source public
```

which will build to the `public` folder.