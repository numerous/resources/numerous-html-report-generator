cat test_status.out > TEST_STATUS

if [[ "$TEST_STATUS" -eq "0" ]]; then PIPELINE="passing"; else PIPELINE="failing"; fi

python3 parse_code_coverage.py | tee code_coverage.out
if [[ "$PIPESTATUS" -eq "1" ]]; then exit 1; fi;

CODE_COVERAGE="$(cat code_coverage.out)"

anybadge --label=pipeline --value=$PIPELINE --file=source/pipeline.svg passing=green failing=red
anybadge --value=$CODE_COVERAGE --file=source/coverage.svg coverage

sphinx-apidoc -o ./source ../src/numerous
sphinx-build -b html source public