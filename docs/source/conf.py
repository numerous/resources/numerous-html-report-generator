# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import sys
import os
# -- Project information -----------------------------------------------------

# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'my-new-project'
copyright = '2023, Author1'
author = 'Author1'
release = '1.0.0'

# CHANGE NAMESPACE (in src), PACKAGE (relative to NAMESPACE), and EXAMPLES (Relative to NAMESPACE)
NAMESPACE = "numerous"
PACKAGE = "html_report_generator"
EXAMPLES = "examples"

sys.path.insert(0, os.path.abspath(f'../../src/{NAMESPACE}'))

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ['sphinx.ext.autodoc', 'sphinx.ext.napoleon', 'sphinx.ext.intersphinx',
              'sphinx_gallery.gen_gallery', 'sphinx.ext.autosummary', 'sphinx.ext.viewcode']

templates_path = ['_templates']
exclude_patterns = []

intersphinx_mapping = {'scipy': ('https://docs.scipy.org/doc/scipy/', None),
                       'numpy': ('https://docs.scipy.org/doc/numpy/', None),
                       'numba': ('https://numba.pydata.org/numba-doc/latest/', None)}


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']
from plotly.io._sg_scraper import plotly_sg_scraper
image_scrapers = (plotly_sg_scraper, )

sphinx_gallery_conf = {
     'doc_module': ('numerous-solver',),
     'examples_dirs': f'../../src/{NAMESPACE}/{PACKAGE}/{EXAMPLES}',   # path to your example scripts,
     'gallery_dirs': 'auto_examples',
     'reference_url': {'numerous-solver': None
      },
     'image_scrapers': image_scrapers,
     'filename_pattern': "",
}
