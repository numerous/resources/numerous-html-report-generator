.. my-new-project documentation master file, created by
   sphinx-quickstart on Fri Jan 27 13:26:30 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the documentation for numerous html report generator!
==========================================

Edit this file to add more custom reST files.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Readme <readme.rst>
   Gallery <auto_examples/index>
   api documentation <modules.rst>



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
